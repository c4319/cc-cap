## market condiderations and input suggestions

In the communitycoins bucketlist a few important issue were recorded concerning price-determinantion of communitycoins. It was listed under "Directories", which are collections of reference material.

- directories
    - coinmarketcap redesign for communitycoins (what parameters are important) ?
      - New metric: Bitcoin normalised marketcap (BNM) = marketcap*21M/distribution 
      - New metric: Target normalised marketcap (TNM) = BNM*7.9B/target_population  (minorities by definition)
    - Size of community, GDP,  Demographics, Coin Distribution, Transaction volume


In our manifest it was mentioned as follows

> Coinmarketcap.com is the most important resource for investors while marketcap (circulating supply*price) is a trivial valuation, especially for community coins. High is better is the main suggestion, but a community is a minority by definition while the community, or let's say the purpose of a coin, is its core value. Community coins deserve a different ranking which we will develop.

And another statement
>Look at what happens next. Coins are traded on exchanges. Exchanges represent a censored market, favouring big over small. The business model of exchanges is to charge transaction fees, not to represent a free market. Obviously coins that generate a large number of transactions remain listed and coins (read communities) with a lower volume gradually get expelled. It is only normal that coin emission and marketing is being "optimized" under the current market conditions. Again, marketcap and volume are wrong incentives to rate community coins. 


This project elaborates on price considerations.

## "Is **Price** just a matter of the free market?"

1. Wether you answer with yes or no, a reservation should be made regarding the free market. Does it still exist, or has it ever existed? Under the guise of regulation, the market seems free, but it is not free at all.  
2. When you consider the market itself, it is comprised of private initiatives (banks, exchanges etc.), with a private bias that diametrically interfers with the cryptocurrency mantra of **decentralisation**. In fact, **exchanges** function the way banks function and severely interfere with the core values of cryptocurrency.

With the current system of central banks, the plasticity of a sovereign currency is mainly achieved through money creation and manipulation of interest rates. If there is less money, then its value increases. The same happens when interest rates increase (the cost to borrow money). With cryptocurrency, the distribution scheme is fixed and cannot be used as a tool to flexibly and artificially change the value of the currency. And who would determine interest rates? There is only one answer: the free market. New rules apply. If you want to stimulate the owner of money to release it, you would need to agree on a price for it or offer collateral, shares or services. But if a currency becomes illiquid because people are hoarding it, other currencies will take its place on the market. [Gresham's law is a monetary principle stating that "bad money drives out good"]. This way you could see a futere where Bitcoin is at the top and too expensive for normal business. One layer below there will be a few coins, well known and globally accepted, such as Litecoin. Below that countrycoins operate because they are apt to become locally best known and trusted. When even these become too illiquid, communitycoins with a more local preference could come into fruitation. For example on the level of states, departments, provinces or even large cities.

If you consider the factors that influence the price of a currency, reputation clearly stands out.
What comes next is manipulation. Probably these can be translated into "natural economical factors" and "deliberate human influencing". At this point we are just investigating what would be the most benificial for human properity. One idea behind bitcoin is that its quantity cannot be manipulated physically. That facilitates the thought experiment to see what happens if nobody would be allowed to regulate or otherwise manipulate the market.  

Lets take the Austrian School of Economics. It emphasizes the importance of free markets and opposes government intervention in the economy. This school elaborately studies market dynamics and intervention historically and theoretically. Of course it would do unjustice to summarize the Austran school in a few words, but generally they conclude that intervention leads to economic inefficiencies and imbalances. Another theory that advocates for limiting human influence on currency prices is the Efficient Market Hypothesis. This theory posits that in a perfectly efficient market, all available information about a currency is already reflected in its price, and any attempt to manipulate the price will quickly be corrected by market forces. Therefore, attempts to deliberately influence currency prices are ultimately futile and can only create temporary distortions that will eventually be corrected.

Off course there are other market theories, but it is very obvious that the current "act of manipulation" is in the hands of very few people that we just have to trust. That is comfortable when things go right but are they? Moreover, it is common knowledge that power corrupts and that maintaining the status quo is utterly irresponsable. There are strong theoretical and historical indicators that point us in the direction of a free market. The tools to implement it are at our disposal.

## Provisional approaches

We can skip Bitcoin, because it has proven itself technically. It did get entangled in the web of government regulation because the infrastructure to isolate a free market from the regulated market has not yet sufficiently evolved. 

We also tend to skip the numerous altcoin experiments that technically challenge Bitcoin while there is no need for that. Bitcoin is close to completion and perfection and serves as a technical bottom-layer. The communitycoins approach is to focus on sovereignty and regional identity. Communitycoins are coins that embed Bitcoin technology in seperate networks with minimal overlap. Coins that don't compete and hence benifit from cooperation and exchange. 

A provisional approach is taken by the team of nationalbitcoin.org, who have bootstrapped the Russian Bitcoin (RUBTC). They focus on its distribution and have prevented (so far) the appearance of RUBTC on the market. While there is no market yet, they are able to dictate (propose) a price, which they have set at 540 dollars per RUBTC.

In fact, any community coin is a proposition in itself. They are proposals to replace fiat currency and bypass banks. The consequence is that 
they are literaly revolutionary, offering a peaceful proposition to turn around the relationship between the government and its citizens. The citizen owns what they have earned: their own money. Parliament determines how this needs to be taxed and what services and expenditures need to be paid for. However, such a revolution cannot happen overnight. It is necessary to experiment with this new concept of money and it's value.

An approach to bypass centralised exchanges is achieved by the development team of Komodo (KMD). They developed a cryptocurrency multi-coin wallet and combined it with an exchange network which they call AtomicDex. Users can swap currencies by offering coins of one currency in their private wallet in exchange for another currency. When the wallet finds a match in the network the wallets begin to exchange directly, peer to peer, without a man in the middle. There is one major problem however, because there is no Bitcoin (or fiat) reference when you exchange two currencies. The price of one currency is expressed in the price of the other. Generally the market price of a cryptocurrency is expressed in Bitcoin or in a Fiat value. It requires endless calculation to determine the value of the trade and it is easy to make a mistake. We propose a solution in a subproject called Market-Merge. For each currency all markets orders are merged in one overview and the order price is expressed in Bitcoin. We will release "Market-Merge" here soon as an open-source backend service
