<?php
/*
	ASKS above $1.000.000 and BUYS below 1 satoshi are eliminated
	Output format markets.txt:
		<coin ticker>\n
			<ask price>\t<coin amount>\t<coin sum>\t<dollar sum>\n
			[...\n]
			.\n
			<bid price>\t<coin amount>\t<bitcoin sum>\t<dollar sum>\n
			[...\n]
		[...]		
*/

define("ROOT","/var/cccap/data/");
$ctx = stream_context_create(array('http' => array('timeout' => 2) ));

$file=file(ROOT."cex_prices.txt");
foreach ($file as $line){
	if (trim($line)!=""){
		$fields=explode(":",$line);
		if (count($fields)==2){$base[$fields[0]]=trim($fields[1]);}
	}
}
// The dex_prices are determined in this routine, but first they are derived from the previous run
$file=@file(ROOT."dex_prices.txt");
foreach ($file as $line){
	if (trim($line)!=""){
		$fields=explode(":",$line);
		if (count($fields)==2){$base[$fields[0]]=trim($fields[1]);}
	}
}

$rates=json_decode(file_get_contents(ROOT."exchange_rates.dat"),1);
$base['USD']=satoshi(1/($rates['rates']['usd']['value']));
$base['USDT']=$base['USD'];
$base['BTC']=1;

$file=file(ROOT."markets");
foreach($file as $line) {
	$record=explode("|",trim($line));
	if (count($record)==1) {
		if (strlen($record[0])>1) {
			$coin=$record[0];
			$coins[]=$coin;
		}
	}
}

$dex_prices="";
foreach ($coins as $coin) {
	$output="";
	unset($a);unset($BUY);unset($SELL);
	foreach($file as $line) {
		$record=explode("|",trim($line));
		if (count($record)==1) {
			if (strlen($record[0])>1) {$current=$record[0];}
		} elseif (($current==$coin)&&(count($record)==2)) {
			$a[$record[0]]=$record[1];
		}
	}
	$n=-1;
	foreach ($a as $markt=>$url){
		list($exchange,$symbol,$basecoin)=explode("-",$markt);
		if (!isset($base[$basecoin])) {
			echo "Kan base $basecoin niet vinden\n";
		} else {
			$factor=trim($base[$basecoin]);
			// The public exchange API's don't bother about 5-min-freq
			$x=@json_decode(strtolower(@file_get_contents($url,0,$ctx)),1);
			if (($exchange=="FREI")||($exchange=="UNNA")) {
				$y=$x['buy'];
				foreach ($y as $line){
					$idx=satoshi($factor*$line['price'])."*".satoshi($line['amount'])."*".$n;
					if ($factor!=1) {$original_price="-".satoshi($line['price']);} else {$original_price="";}
					$BUY[$idx]="$markt-BUY$original_price";
					$n++;		
				}
				$y=$x['sell'];
				foreach ($y as $line){
					$idx=satoshi($factor*$line['price'])."*".satoshi($line['amount'])."*".$n;
					if ($factor!=1) {$original_price="-".satoshi($line['price']);} else {$original_price="";}
					$SELL[$idx]="$markt-SELL$original_price";
					$n++;		
				}
			} elseif ($exchange=="XEGGEX") {
				$y=$x['bids'];
				foreach ($y as $line){
					$idx=satoshi($factor*$line['price'])."*".satoshi($line['quantity'])."*".$n;
					if ($factor!=1) {$original_price="-".satoshi($line['price']);} else {$original_price="";}
					$BUY[$idx]="$markt-BUY$original_price";
					$n++;		
				}
				$y=$x['asks'];
				foreach ($y as $line){
					$idx=satoshi($factor*$line['price'])."*".satoshi($line['quantity'])."*".$n;
					if ($factor!=1) {$original_price="-".satoshi($line['price']);} else {$original_price="";}
					$SELL[$idx]="$markt-SELL$original_price";
					$n++;		
				}
			} elseif ($exchange=="XASSETS") {
				$param['pairs']=array("$url");
				$param['limit']=25;
				$ch = curl_init('https://exchange-assets.com/api/public/v2/order_book/');
				curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
				curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($param));
				$x = json_decode(curl_exec($ch),1);
				curl_close($ch);
				$y=$x[0]['bids'];
				foreach ($y as $line){
					$idx=satoshi($factor*$line[0])."*".satoshi($line[1])."*".$n;
					if ($factor!=1) {$original_price="-".satoshi($line[0]);} else {$original_price="";}
					$BUY[$idx]="$markt-BUY$original_price";
					$n++;		
				}
				$y=$x[0]['asks'];
				foreach ($y as $line){
					$idx=satoshi($factor*$line[0])."*".satoshi($line[1])."*".$n;
					if ($factor!=1) {$original_price="-".satoshi($line[0]);} else {$original_price="";}
					$SELL[$idx]="$markt-SELL$original_price";
					$n++;		
				}			
			}
		}
	}

	/* Just read the atomicdex-orders [output from atomicdex_markets] */
	if (file_exists(ROOT."catdex_sells_$coin")) {
		$a=json_decode(file_get_contents(ROOT."catdex_sells_$coin"),1);
		foreach ($a as $one => $value){$SELL[$one]=$value;}
	}
	if (file_exists(ROOT."catdex_buys_$coin")) {
		$a=json_decode(file_get_contents(ROOT."catdex_buys_$coin"),1);
		foreach ($a as $one => $value){$BUY[$one]=$value;}
	}

	/* Put them in an integrated list */
	krsort($SELL);
	krsort($BUY);
	$sum=$bitcoin=0;
	foreach ($SELL as $line =>$markt){
		list($price,$amount,$x)=explode("*",$line);
		if ($price>0.00000001) {$sum+=satoshi($amount);$bitcoin+=satoshi($price*$amount);}
	}
	$stat[$coin]['coins_for_sale']=$sum;
	foreach ($SELL as $line =>$markt){
		list($price,$amount,$x)=explode("*",$line);
		$price=satoshi($price);
		$amount=satoshi($amount);
		$sum=satoshi($sum);
		$bitcoin=satoshi($bitcoin);
		$dollar=floor($bitcoin/$base['USD']);
		if (($dollar<1000000) && ($price>0.00000001)) {
			$lowest_ask=$price;
			$output.="$price\t$amount\t$sum\t$bitcoin\t$dollar\t$markt\n";
		}
		$sum-=$amount;
		$bitcoin-=$price*$amount;		
	}
	$highest_bid=0;
	$output.=".\n";
	$sum=$bitcoin=0;
	foreach ($BUY as $line =>$markt){
		list($price,$amount,$x)=explode("*",$line);
	}
	foreach ($BUY as $line =>$markt){
		list($price,$amount,$x)=explode("*",$line);
		list($exchange,$basecoin,$relcoin)=explode("-",$markt);
		if ($highest_bid==0) {
			// rel-coins with a price < 0.00000001 cannot influence the price of the base-coin
			// because their ask price can be below the 1 satoshi wall
			if ($base[$relcoin]>=0.00000003) {$highest_bid=$price;}
		}
		$sum+=$amount;
		$sum=satoshi($sum);
		$price=satoshi($price);
		$bitcoin+=($amount*$price);
		$bitcoin=satoshi($bitcoin);
		$amount=satoshi($amount);
		$dollar=floor($bitcoin/$base['USD']);
		if ($price>=0.00000001) {$output.="$price\t$amount\t$sum\t$bitcoin\t$dollar\t$markt\n";}
	}

	$stat[$coin]['price']=satoshi(($highest_bid+$lowest_ask)/2);
	$stat[$coin]['highest_bid']=$highest_bid;
	$stat[$coin]['lowest_ask']=$lowest_ask;
	$stat[$coin]['bitcoins_for_sale']=$bitcoin;
	if ($dex_prices!="") {$dex_prices.="\t";}
	$dex_prices.="$coin:".satoshi(($highest_bid+$lowest_ask)/2);

	$coin=strtolower($coin);
	file_put_contents(ROOT."{$coin}.txt",$output);
	
}
file_put_contents(ROOT."prices.txt",$dex_prices);
file_put_contents(ROOT."history_prices.txt", time()."\t$dex_prices\n",FILE_APPEND);
file_put_contents(ROOT."stat.txt",json_encode($stat));
file_put_contents(ROOT."history_stat.txt",time()."\t".json_encode($stat)."\n",FILE_APPEND);
function satoshi($x){ return substr(number_format((float)$x,8,".",""),0,10);}
?>

